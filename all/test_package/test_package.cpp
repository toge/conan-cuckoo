#include <iostream>
#include <string>

#include "libcuckoo/cuckoohash_map.hh"

int main() {
    libcuckoo::cuckoohash_map<int, std::string> Table;

    for (int i = 0; i < 100; ++i) {
        Table.insert(i, "Hello");
    }
    return 0;
}
